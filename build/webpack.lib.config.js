const path = require('path');



module.exports = {
    resolve: {
        extensions: ['.js', '.ts', '.json'],
    },
    devtool: 'source-map',// 打包出的js文件是否生成map文件（方便浏览器调试）
    mode: 'production',
    entry: {
        'echatim-sdk': './src/echatim.ts',
    },
    output: {
        filename: '[name].js',// 生成的fiename需要与package.json中的main一致
        path: path.resolve(__dirname, '../dist'),
        libraryTarget: process.env.platform === 'web' ? 'window' : 'umd',
        library: "imsdk"
    },
    module: {
        rules: [
            {
                test: /\.tsx?$/i,
                use: [

                  {
                    loader: 'ts-loader',
                    options: {
                        // configFile: path.resolve(__dirname, './tslint.json'),
                        configFile: path.resolve(__dirname, '../tslint.json')
                    },
                  },

                  {
                    loader: 'js-conditional-compile-loader',
                    options: {
                      isDebug: process.env.NODE_ENV === 'development', // optional, this is default
                      WEBAPP: process.env.platform === 'web', // any name, used for /* IFTRUE_WEBAPP ...js code... FITRUE_WEBAPP */
                      WXAPP: process.env.platform === 'wx', // any name, used for /* IFTRUE_WXAPP ...js code... FITRUE_WXAPP */
                      // RNAPP: process.env.platform === 'rn', // any name, used for /* IFTRUE_RNAPP ...js code... FITRUE_RNAPP */
                    }
                  },

                ],
                exclude: /node_modules/
            }
        ],
    },
    plugins: [],
};
