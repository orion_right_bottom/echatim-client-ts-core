#### TypeScript客户端核心SDK 说明

官网: http://www.echatim.cn

文档: http://doc.echatim.cn:58083

核心SDK使用Typescript开发, 使用js-conditional-compile-loader插件条件编译出微信小程序端, Web网页端使用的js sdk

打包出微信小程序使用的sdk:
```bash
yarn wxlib # 将会在 dist 目录生成echatim-sdk.js 文件.
```


打包出Web网页端使用的sdk:
```bash
yarn weblib # 将会在 dist 目录生成echatim-sdk.js 文件.
```


打包出ReactNative端使用的sdk:
```bash
yarn rnlib # 将会在 dist 目录生成echatim-last.zip TypeScript打包文件.
```




#### 版本记录:
##### v 1.02 - 2020-06-28

* 加入微信小程序平台支持, 包括通讯, 文件上传等功能.


##### v 1.01 - 2020-05-24
* 第一版源码发布
