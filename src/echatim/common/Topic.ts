/**
 * @author kong <androidsimu@163.com>
 * create by 2019/2/21 9:28
 **/
export class Topic {
    static prefix = 'topic.';
    static httpPrefix = '/v1/';

    static CONNECTION = class {
        static topic_name = Topic.prefix + "connection";
        static base_uri = Topic.httpPrefix + "connection";

        static METHOD = class {
            static AUTHORITY_REQUEST = "authority_request";
        }
    };

    static APP_USER = class {
        static topic_name = Topic.prefix + "user";
        static base_uri = Topic.httpPrefix + "user";
        static METHOD = class {
            static ADD = "add";
            static UPDATE = "update";
            static LIST = "list";
            static GET = "get";
            static UPDATE_TOKEN = "update_token";
            static REFRESH_TOKEN = "refresh_token";
        }
    };

    // 通过服务器向客户端发送消息的prefix
    static APP_UPSTREAM_MESSAGE = class {
        static topic_name = Topic.prefix + "upstream_message";
        static base_uri = Topic.httpPrefix + "upstream_message";

        static METHOD = class {
            static SEND = "send";
            static BATCH_SEND = "batch_send";
            static SEND_ACK_TO_SERVER = "sendack_toserver";
        }
    };


    // 服务器向客户端发送消息的prefix
    static APP_DOWNSTREAM_MESSAGE = class {
        static topic_name = Topic.prefix + "downstream_message";
        static base_uri = Topic.httpPrefix + "downstream_message";
        static METHOD = class {
            static SEND_MESSAGE_TO_CLIENT = "sendmessage_toclient";
            static SEND_EVENT_TO_CLIENT = "sendevent_toclient";
        }
    };


    static APP_CLIENT = class {
        static topic_name = Topic.prefix + "client";
        static base_uri = Topic.httpPrefix + "client";
        static METHOD = class {
            static USER_ONLINE = "user_online"; // 列出用户的在线状态
            static ADD_ONLINE_MONITOR = "add_online_monitor";// 设置用户的在线监听
        }
    };

    static APP_ROOM = class {
        static topic_name = Topic.prefix + "room";
        static base_uri = Topic.httpPrefix + "room";
        static METHOD = class {
            static ADD = "add"; // 添加群
            static UPDATE = "update"; // 更新群信息
            static LIST = "list"; // 列出群信息
            static DEL = "del"; // 删除群
            static GET = "get"; // 获取单个群信息
            static LIST_JOIN = "list_join"; // 列出当前用户加入的群
            static LIST_MEMBER = "list_member"; // 列出群信息/群成员
            static ADD_USER = "add_user"; // 添加群用户
            static DEL_USER = "del_user"; // 删除群用户
        }
    };


    static APP_USER_RELATION = class {
        static topic_name = Topic.prefix + "user_relation";
        static base_uri = Topic.httpPrefix + "user_relation";
        static METHOD = class {
            static ADD_FRIEND = "add_friend"; // 添加好友
            static DEL_FRIEND = "del_friend"; // 删除好友
            static LIST_FRIENDS = "list_friends"; // 列出好友列表
            static GET_FRIEND = "get_friend"; // 获取好友
            static MODIFY_BLACKLIST_FORBID = "add_blacklist";// 添加/移除黑名单, 添加/移除黑禁言
            static LIST_BLACKLIST_FORBID = "list_blacklists"; // 列出黑名单, 禁言列表
            static MODIFY_ALIAS = "modify_alias";// 修改IM好友别名
        }
    };

    // 通过服务器向客户端发送消息的prefix
    static APP_HISTORY_MESSAGE = class {
        static topic_name = Topic.prefix + "history";
        static base_uri = Topic.httpPrefix + "history";
        static METHOD = class {
            static LIST_MESSAGE = "list_message"; // 列出历史消息
            static LIST_SESSION = "list_session"; // 列出用户历史会话列表
        }
    }

}

// export const ALL_prefixS = [prefix_CONNECTION.name, prefix_APP_USER.name];


