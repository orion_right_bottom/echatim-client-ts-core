import ProtocolMessage from "../protocol/ProtocolMessage";

export default class RoomAddUserForm extends ProtocolMessage {
    public rid = 0; // 群ID
    public owner = ""; // 群主auid
    public members = new Array<string>(); // 群成员, 无需再加owner自己的账号
    public needAgree = 0; // 拉群时是否需要确认.
    public inviteText = ""; // 邀请文字
    public ex = ""; // 扩展字段
}
