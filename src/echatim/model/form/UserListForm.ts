import ProtocolMessage from "../protocol/ProtocolMessage";

export default class UserListForm extends ProtocolMessage {
    public auids = new Array<string>(); // 要获取的用户uid
}
