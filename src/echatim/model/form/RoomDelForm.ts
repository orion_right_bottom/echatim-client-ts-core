import ProtocolMessage from "../protocol/ProtocolMessage";

export default class RoomDelForm extends ProtocolMessage {
    public rid = 0; // 群ID
    public owner = ""; // 群主auid
}
