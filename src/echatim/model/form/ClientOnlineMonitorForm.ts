import ProtocolMessage from "../protocol/ProtocolMessage";

export default class ClientOnlineMonitorForm extends ProtocolMessage {
    public auid = ""; // 监听者用户auid
    public targetAuids = new Array<string>(); // 要监听的用户auids
}
