import ProtocolMessage from "../protocol/ProtocolMessage";

export default class UserBlacklistModifyForm extends ProtocolMessage {
    public auid = ""; // 要添加的用户uid
    public targetAuid = ""; // 要添加的朋友uid
    public type = ""; // ADD_BLACKLIST:添加黑名单;ADD_FORBID:添加禁言;REMOVE_BLACKLIST:移除黑名单;REMOVE_FORBID:移除禁言;
}

