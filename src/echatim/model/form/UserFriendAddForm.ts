import ProtocolMessage from "../protocol/ProtocolMessage";

export default class UserFriendAddForm extends ProtocolMessage {
    public auid = ""; // 要添加的用户uid
    public targetAuid = ""; // 要添加的朋友uid
    public type = ""; // ADD:直接加好友;APPLY:直接加好友;AGREE:同意加好友;REJECT:拒绝加好友;
    public alias = ""; // 添加好友时的备注名
    public applyText = ""; // 请求加好友对应的请求消息(此信息在接受好友添加请求的时候显示)
}

