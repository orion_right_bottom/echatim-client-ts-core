import ProtocolMessage from "../protocol/ProtocolMessage";

export default class UserFriendAliasForm extends ProtocolMessage {
    public auid = ""; // 用户uid
    public targetAuid = ""; // 要编辑的朋友uid
    public alias = ""; // 要修改的朋友备注名
}

