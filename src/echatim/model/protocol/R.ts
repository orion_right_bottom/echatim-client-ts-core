
export default class R<T> {
    /**
     * 业务错误码
     */
    public code = 0;
    /**
     * 结果集
     */
    public data: T|any = {};
    /**
     * 描述
     */
    public msg = '';
    static ok(): R<any> {
        return new R();
    }
    static failed(): R<any> {
        const r:R<any> = new R();
        r.code = -1;
        return r;
    }
}
