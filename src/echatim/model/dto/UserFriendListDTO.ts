
export default class UserFriendListDTO  {
    public auid = ""; // 好友的auid
    public name = ""; // 好友名字
    public alias = ""; // 好友备注名
    public avatar = ""; // 好友头像
    public sign = ""; // 好友签名

    public createTime = new Date(0); // 创建时间
}
