
export default class MessageListDTO  {
    public id = 0; // 消息id
    public way = ""; // 发送方式(P2P,P2R,P2HR)
    public fromUser = ""; // 消息发送者
    public fromUserName = ""; // 消息发送者名称
    public toTarget = ""; // 消息发送对象
    public toMe = 0; // 消息是不是发给我的(0:自己发出去的消息; 1. 别人发给我的消息)
    public type = ""; // 消息类型
    public body:any = {}; // 消息内容(JSON格式)
    public avatar = ""; // 用户头像

    public createTime = new Date(0); // 创建时间
}
