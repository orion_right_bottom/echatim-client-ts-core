

/**
 * 阿里云OSS AliOssUploadRequestDTO
 *
 * @author Daizhiqiang
 * @date 2019/11/22 18:07
 */

export default class AliOssUploadRequestDTO {
    public  accessid = "";
    public  policy = "";
    public  signature = "";
    public  dir = "";
    public  host = "";
    public  expire = 0;
    public  callback = "";
}


