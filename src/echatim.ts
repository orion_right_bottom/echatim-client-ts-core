import { echatIMSDK } from "./echatim/core/sdk/EChatIMSDK";
import Logger from "./echatim/log/Logger";
import EChatIMApis from "./echatim/core/apis/EChatIMApis";

/**
 *  echatIMSDK module package entry
 * */
if(window !== undefined){
    window['im'] = echatIMSDK;
}




export {echatIMSDK as im, EChatIMApis as apis, Logger as log};

